# Cupcakes Bakery

Responsive webpage on baked products site demo here : https://rossvincentdiaz12.gitlab.io/cupcakes-bakery

### Prerequisites

Bootstrap Studio

### Installing

Intall your Bootstrap Studio
then open the .bsdesign file 

## Built With

* [BOOTSTRAP STUDIO](https://bootstrapstudio.io/tutorials/) 

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/rossvincentdiaz12/cupcakes-bakery) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details



